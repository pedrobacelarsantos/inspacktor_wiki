---
title: "About"
date: 2021-10-24T05:35:25-03:00
draft: false
---
# insPACKtor, a mais de 1 ano inspecionando
## Venha conhecer um pouco melhor nossa trajetória

![alt text](https://media.discordapp.net/attachments/864300101489000478/866399622603735060/Qualidade_e_Controle_de_Qualidade.png?width=693&height=390)



A insPACKtor surgiu na disciplina 0303410 (Desenvolvimento Integrado de Produtos), ministrada pelo Prof. Dr. Eduardo de Senzi Zancul. A disciplina tinha como foco a resolução multidisciplinar de um problema real de um parceiro empresarial da disciplina. Com o término da disciplina, alguns dos membros do projeto decidiram continuar as pesquisas e o caminho natural foi a abertura de uma startup. Para adentrar melhor nesse mundo de empreendedorismo, os membros passaram por um processo seletivo e foram aprovados na maior incubadora da América Latina, o CIETEC. 


Após o período de pré-incubação no CIETEC, a startup participou de um edital de projetos da Ambev e foi aprovada para a execução de projeto de manutenção preditiva sonora!

