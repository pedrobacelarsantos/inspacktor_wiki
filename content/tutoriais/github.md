---
title: "Github na Raspberry Pi!"
date: 2021-10-24T05:47:51-03:00
draft: false
capa: "https://rockcontent.com/br/wp-content/uploads/sites/2/2020/03/github.jpg"
descricao: "Aprenda como instalar o Github na Raspberry Pi e como usar os comandos básicos!"
---

Funciona basicamente como no seu PC, a grande diferença é que não é possível usar o github desktop porque a raspberry é 32bits, então iremos precisar usar o cmd da raspberry mesmo do jeito raiz primeiramente, instale o git na raspberry:

```python
sudo apt-get install git
```

Em seguida verifique a versão do git instalado usando:

```python
git --version
```

### Algumas Configurações

Para escolher um nome e email use os seguintes comandos

```python
git config --global user.name "<USERNAME>"
git config --global user.email "<SEU EMAIL>"
```