---
title: "Comunicação Serial UART na Raspberry Pi"
date: 2021-10-24T05:47:51-03:00
draft: false
capa: "https://www.distrelec.biz/Web/WebShopImages/landscape_large/87/fa/raspberry-pi-b-plus-30001887fa.jpg"
descricao: "Aprenda como configurar a comunicação serial na Raspberry Pi!"
---

Essa configuração assume os pinos 8 com TX e 10 como RX.

1. Para configurar a serial é preciso editar o arquivo “inittab”, localizado em “/etc/inittab”. Para habilitar a serial deve-se comentar a linha “T0:23:respawn:/sbin/getty -L ttyAMA0 115200 vt100”. Para abrir este arquivo digite um dos seguinte comando no Terminal:

```python
sudo leafpad /etc/inittab
```

ou digite (recomendável)

```python
sudo nano /etc/inittab
```

1. Caso o arquivo anterior não exista ou você não consiga acessar, utilize um dos seguintes comandos e adicione ao final a seguinte linha “enable_uart=1”

```python
sudo leafpad /boot/config.txt
```

ou 

```python
sudo nano /boot/config.txt
```

1. Em seguida desabilite a serial com os seguintes comandos:

```python
sudo systemctl stop serial-getty@ttyS0.service
sudo systemctl disable serial-getty@ttyS0.service
```

2. Após isso, iremos remover o "console=serial0,115200” do arquivo /boot/cmdline.txt, para isso execute um dos seguintes comandos e remova a parte destacada anteriormente

```python
sudo leafpad /boot/cmdline.txt
```

ou digite:

```python
sudo nano /boot/cmdline.txt
```

3. Salve o arquivo anterior e realize o "Reboot" da Raspberry

OBS INTERESSANTE: É possível comunicar dois dispositivos com comunicação serial mesmo se ambos estão sendo alimentados com fontes diferentes (RAspberry com seu carregador, e arduino conectado na USB do PC). ENTRETANTO, ambos precisam estar conectados no mesmo GROUND!