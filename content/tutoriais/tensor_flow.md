---
title: "TensorFlow na Raspberry Pi!"
date: 2021-10-24T05:47:51-03:00
draft: false
capa: "http://devclass.com/wp-content/uploads/2018/08/Tensorflow.jpg"
descricao: "Aprenda como instalar o TensorFlow na Raspberry Pi!"
---

**ATENÇÃO:** Lembrando que o tensorflow opera na versão **1.16.2 do numpy**

Se você preferir é possível criar um virtual enviorment, mas como iremos rodar a man com essa versão de TensorFlow e demais bibliotecas iremos fazer direto na main da Raspberry.

1. Rode os seguintes comandos na sua respectiva ordem (comandos baseado em: [https://github.com/PINTO0309/Tensorflow-bin/#usage:](https://github.com/PINTO0309/Tensorflow-bin/#usage:))

```python
sudo apt-get install -y libhdf5-dev libc-ares-dev libeigen3-dev
python3 -m pip install keras_applications==1.0.8 --no-deps
python3 -m pip install keras_preprocessing==1.1.0 --no-deps
python3 -m pip install h5py==2.9.0
sudo apt-get install -y openmpi-bin libopenmpi-dev
sudo apt-get install -y libatlas-base-dev
python3 -m pip install -U six wheel mock
```

1. Agora iremos escolher a versão do tensor que queremos baixar, primeiramente acesse: 

[Releases · lhelontra/tensorflow-on-arm](https://github.com/lhelontra/tensorflow-on-arm/releases)

2. Vá para a versão desejada (nesse caso a 2.3.0) e copie o link do último arquivo terminado com 7l.whl


3. De volta para o terminal da raspberry digite wget seguido do link copiado, para a versão 2.3.0 fica como segue:

```python
wget https://github.com/lhelontra/tensorflow-on-arm/releases/download/v2.3.0/tensorflow-2.3.0-cp37-none-linux_armv7l.whl
```

4. Em seguida desisntalamos versões anteriores de qualquer outro tensorflow

```python
python3 -m pip uninstall tensorflow
```

5. E por fim realizamos a instalação. Apenas lembrar de colocar o nome certo para instalação de acordo com a versão de TensorFlow escolhida, no caso para a 2.3.0 fica como segue:

```python
python3 -m pip install tensorflow-2.3.0-cp37-none-linux_armv7l.whl
```

6. Restart o seu terminal e faça o seguinte teste

```python
python3
import tensorflow
tensorflow.__version__
```