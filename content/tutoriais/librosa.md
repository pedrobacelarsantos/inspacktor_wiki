---
title: "Librosa na Raspberry Pi"
date: 2021-10-24T05:47:51-03:00
draft: false
capa: "https://librosa.org/doc/main/_images/librosa-feature-melspectrogram-1.png"
descricao: "Aprenda como instalar o LIBROSA na Raspberry Pi!"
---
IMPORTANTE: O TensorFlow 2.3.0 opera numa versão mais antiga do numpy, por isso é necessário primeiro instalar o TensorFlow para depois instalar o Librosa. Além disso, dependendo da aplicação, o Librosa não se faz necessário, pois todo o processamento já é feito direto no TensorFlow.

1. Use os seguintes comandos no terminal:

```python
sudo apt-get install llvm
```

2. Tenha certeza do caminho correto do llvm (/usr/bin/llvm-config). Para isso use o código:

```python
which llvm-config
```

3. Continue a instalação digitando os seguintes comandos

```python
LLVM_CONFIG=/usr/bin/llvm-config pip3 install llvmlite==0.32
```

4. ATENÇÃO PARA A INSTALÇÃO DO NUMPY:

```python
pip3 install numpy==1.21.1 numba==0.49
```

5. Instalação do librosa:

```python
pip3 install librosa
```

6. Se ao tentar dar um import do librosa receber um erro do colorama, ajuste a versão da biblioteca, atualmente a 0.3.9 já suficiente

```python
pip3 install colorama==0.3.9
```

ATENÇÃO: Lembrando que o tensorflow 2.3.0 idealmente roda com versão **1.16.2 do numpy**, logo caso você não vá mais usar o Librosa, instale o numpy na versão 1.16.2 para o tensorflow rodar sem possibilidade de problemas.